import sys


def hello():
    print('Hello world.')
    

def factorial(x):
    if x > 0:
        return x * factorial(x - 1)
    else:
        return 1


def my_max(x, y):
    if x > y:
        return x
    else:
        return y


def args_desc():
    for i in range(len(sys.argv) - 1, 0, -1):
        print(sys.argv[i])

    for i in sys.argv[1::][::-1]:
        print(i)

    for i in reversed(sys.argv[1::]):
        print(i)

    for i in sys.argv[:0:-1]:
        print(i)



def multi(x):
    n = int(x)

    for i in range(1, 11):
        print(i, i * n)


def maxminavg(numbers_str):
    numbers = [int(i) for i in numbers_str]

    print(max(numbers), min(numbers), sum(numbers) / len(numbers))

def pine(height):
    for i in range(0, height):
        for j in range(1, height - i):
            print(' ', end='')

        for s in range (1, i * 2):
            print('*', end='')

        print()

    for j in range (1, height - 1):
        print (' ', end='')

    print ('*')


hello()
factorial(5)
my_max(5,7)
args_desc()
multi(5)
maxminavg(sys.argv[1::])
pine(8)
