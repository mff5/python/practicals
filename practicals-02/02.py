import math


def multi(x):
    """
    Prints out a multiplication table
    :param i: the base for the table
    :return: nothing
    """
    for i in range(11):
        print('{0:2d} * {1:2d} = {2:3d}'.format(i, x, i * x))


def pathelem(path):
    """
    Prints out particular path elements passed as the argument
    :type path: str
    :param path: the string to be parsed
    :return: nothing
    """
    elements = path.split('/')
    elements.pop(0)
    filename = elements.pop(-1)
    if '.' not in filename:
        elements.append(filename)
        filename = ''

    for i in elements:
        print(i)

    print()
    if filename != '':
        print('Filename:  ', filename.split('.')[0])
        print('Extension: ', filename.split('.')[1])


def nnn(n):
    """
    Prints value of n+nn+nnn for a given n. Note here that n is string,
    so 3 * n is nnn (still a string) that is eventually transformed to
    int.
    :type n: str
    :param n: the input number
    :return: the value of n+nn+nnn
    """
    n1 = int(3 * n)
    n2 = int(2 * n)
    n3 = int(n)
    print(n1 + n2 + n3)


def erasto(n):
    """
    Applies the Erasthotenes algorithm up to number n
    :param n: the high limit on the numbers
    :return: the list of primes up to n
    """
    tresh = int(math.sqrt(n))

    ints = list(range(n + 1))

    # removing 0 and 1
    ints.pop(0)
    ints.pop(0)

    i = 2
    idx = 1

    while i <= tresh:
        for j in range(2, n // i + 1):
            try:
                ints.remove(i * j)
            except ValueError:
                pass

        i = ints[idx]
        idx = idx + 1

    print(ints)

    return ints


def countprimes(low, high):
    if low > high:
        low, high = high, low

    allprimes = erasto(high)
    i = 0

    for i in range(len(allprimes)):
        if allprimes[i] >= low:
            break

    print(f'Number of primes between {low} and {high}: {len(allprimes) - i}')
    print(allprimes[i:])


def perms(pref, s):
    if len(s) == 1:
        print(pref + s)
    else:
        for i in range(len(s)):
            c = s[i]
            perms(pref + c, s[:i] + s[i + 1:])


def printtreeline(linelevel, number, level):
    for i in range(number):
        if linelevel == 0:
            print((2 ** (level - 1) - 1) * ' ', end='')
            print('*', end='')
            print((2 ** (level - 1)) * ' ', end='')
        else:
            print((2 ** (level - 1) - linelevel - 1) * ' ', end='')
            print('*', end='')
            print((linelevel * 2 - 1) * ' ', end='')
            print('*', end='')
            print((2 ** (level - 1) - linelevel) * ' ', end='')

    print()


def printsubtrees(level, number):
    if level > 2:
        for j in range(2 ** (level - 2)):
            printtreeline(j, number, level)

        printsubtrees(level - 1, number * 2)

    else:
        printtreeline(0, number, 2)
        printtreeline(0, number * 2, 1)


def printtree(level):
    if level == 0:
        print()
    elif level == 1:
        print('*')
    else:
        printsubtrees(level, 1)


multi(10)
pathelem('/home/user/dir1/file.txt')
nnn('15')
countprimes(10000, 9900)
perms('', 'abcd')
printtree(7)
