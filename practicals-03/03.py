import random


def multi():
    """
    Creates a list containing tuples with elements in the multiplication
    table, i.e., [(1,1,1), (1,2,2),...(10,9,90), (10,10,100)]
    """
    print([[i, j, i * j] for i in range(1, 11) for j in range(1, 11)])


def multi2():
    """
    Alternative version of multi()
    """
    print([[i, j, i * j] for i in range(1, 11, 2) for j in range(1, 11)])
    print([[i, j, i * j] for i in range(2, 11, 2) for j in range(1, 11)])


def calc(data):
    """
    A simple calculator that accepts the input in the reverse Polish
    notation (i.e., the postfix notation) the expressions are accepted
    as commandline arguments
    :param data: The input expression to be evaluated
    :return: nothing
    """
    parts = data.split(' ')
    numbers = list()

    for item in parts:
        try:
            numbers.append(int(item))
        except ValueError:
            numbers.append(eval(str(numbers.pop(-1)) + item + str(numbers.pop(-1))))

    print(numbers[0])


def num_char(data):
    """
    Prints out the number of occurrences of individual characters in
    the given string
    :param data: the inpur string
    :return: nothing
    """
    chars = dict()

    for ch in data:
        try:
            chars[ch] = chars[ch] + 1
        except KeyError:
            chars[ch] = 1

    for key, value in chars.items():
        print(key + ': ' + ('once.' if value == 1 else str(value) + ' times.'))


def num_char2(data):
    """
    Alternative version of numchar(data)
    """
    print({c: data.count(c) for c in data})


def find_min(data):
    """
    Finds the index of a minimal element in the input data.
    :param data: the input data (iterable)
    :return: the index of the minimal element
    """
    minimum = data[0]
    idx = 0

    for i in range(len(data)):
        if data[i] < minimum:
            minimum = data[i]
            idx = i

    return idx


def select_sort(data):
    """
    Sorts and prints out the input data using the select-sort algorithm.
    :param data: the input date to be sorted
    :return: nothing
    """
    for i in range(len(data)):
        idx = find_min(data[i:])
        data[i], data[idx + i] = data[idx + i], data[i]

    print(data)


def select_sort2(data):
    """
    Alternative version of select_sort(data) using the built-in min function
    """
    for i in range(len(data)):
        idx = data.index(min(data[i:]), i)
        data[i], data[idx] = data[idx], data[i]

    print(data)


def heapify(data):
    """
    Reorders elements of the data array to form a heap
    :param data: the input data
    :return: nothing
    """
    for i in range(1, len(data)):
        idx = i
        while data[i] > data[i // 2]:
            data[i], data[i // 2] = data[i // 2], data[i]
            i = i // 2


def sift_down(data, upto):
    """
    Sifts-down the top element of the heap into the correct place.
    It is used during the heapsort algorithm.

    :param data: The input array to be modified
    :param upto: The limit up to which the sifted element is moved
    :return: nothing
    """
    i = 0
    while i < (upto // 2):
        if upto > (i * 2 + 2):
            if data[i] < max(data[i * 2 + 1], data[i * 2 + 2]):
                if data[i * 2 + 1] > data[i * 2 + 2]:
                    data[i], data[i * 2 + 1] = data[i * 2 + 1], data[i]
                    i = i * 2 + 1
                else:
                    data[i], data[i * 2 + 2] = data[i * 2 + 2], data[i]
                    i = i * 2 + 2
            else:
                break

        elif upto > (i * 2 + 1) and data[i] < data[i * 2 + 1]:
            data[i], data[i * 2 + 1] = data[i * 2 + 1], data[i]
            i = i * 2 + 1
        else:
            break


def heapsort(data):
    """
    Sorts and prints out the data stored in the data parameter
    :param data: The array containing the data to be sorted
    :return: nothing
    """
    heapify(data)

    for i in range(len(data) - 1, 0, -1):
        data[0], data[i] = data[i], data[0]
        sift_down(data, i)

    print(data)


multi()
multi2()
calc('1 2 3 4 * * -')
num_char('hello, how are you, I am here just to make you happy!')
num_char2('hello, how are you, I am here just to make you happy!')
random.seed()
data = random.sample(range(100), 20)
select_sort(data.copy())
select_sort2(data.copy())
heapsort(data.copy())
